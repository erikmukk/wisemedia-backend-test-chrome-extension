# What is this project

This is a Chrome extension, created by Erik Mukk for Wisemedia backend developer internship position.

# Chrome extension for time tracking

This is a time tracking extension for Google Chrome. 
With this extension you automatically synchronize your results with Toggl.com.

# Adding it to Chrome

Firstly, you need to download/clone this project to your computer.  
Secondly, open your Chrome browser, type in "chrome://extensions" and open it.  
Thirdly, enable "Developer mode".  
Finally, click on "Load unpacked" and choose the folder, where you downloaded this extension.

# Usage

An icon has appeared on your Chrome window on top right corner. Click on it, log in with your Toggl e-mail and password.
When successfully logged in, set up your pomodoro length, break lengths, set a goal, choose a workspace and press "Start". 
To stop the timer after your time tracking is done, simply click on "Break and reset".  
You will be notified when you start the timer, when there is a break, when the break ends and when something has gone wrong.  

The state of current pomodoro ("ON", "OFF", "BREAK") is displayed the whole time on the top right corner of your Chrome window.

Every time entry will be automatically synced with Toggl, via their API. 

# Some problems I ran into

When making GET requests with jQuery ajax, I could not get in touch with Toggl API with conventional methods. So I had to 
create a CORS proxy website (and host it on Heroku). 
No sent and requested data is stored there and you can read more about that here: https://murmuring-basin-43724.herokuapp.com/ .  

Also, on 31st of March 2019, when I tested extension once more (just in case), there has been something weird going on with Toggl API.
When I started the timer via my extnesion, then Toggl started tracking time not from 0 seconds, but from 8 seconds. So lets say that I let my timer
run for 1 minute and having Toggl website open, then I saw the following anomalies.  
When I started the timer: ![Screenshot 1](https://gyazo.com/c73ea98080762dc3e38462713c6dbaa8.png "Screenshot 1")   
When I ended the timer: ![Screenshot 2](https://gyazo.com/872fc577263525d88071d0c9b5869973.png "Screenshot 2")   
What Toggl logged: ![Screenshot 3](https://gyazo.com/b612d060fe70b4a90f50676b253ddaa1.png "Screenshot 3")   
Even though I set my timer for 1minute, Toggl shows 1minute and 2seconds, because API call takes some time and these
anomalies, that I encountered, did not have any influence on actual time tracking and logging.