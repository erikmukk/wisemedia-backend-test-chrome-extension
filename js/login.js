let loginButton;
let passwordField;
let usernameField;
let messageP;
document.addEventListener("DOMContentLoaded", function () {

   usernameField = document.getElementById("username");
   passwordField = document.getElementById("password");
   loginButton = document.getElementById("login_button");
   messageP = document.getElementById("messageP");

   loginButton.addEventListener('click', function (e) {
       e.preventDefault();
       let encodedCreds = btoa(usernameField.value + ":" + passwordField.value);
       loginRequest(encodedCreds);
   });

});

// Get Toggl API key
function loginRequest(encodedCreds) {
    loginButton.disabled = true;
    messageP.innerHTML = "Logging in! Please wait";
    messageP.style.display = 'block';
    $.ajax({
        type: "GET",
        // My own CORS proxy to get this thing working
        url: "https://murmuring-basin-43724.herokuapp.com/https://www.toggl.com/api/v8/me",
        beforeSend:
            function (xhr) {
                xhr.setRequestHeader('Authorization', "Basic "+encodedCreds);
                xhr.setRequestHeader("Content-Type", "application/json");
        },
        // If successful, user can begin their time-tracking
        success:
            function(res) {
                let base64EncryptedToken = btoa(res['data']['api_token'] + ":api_token");
                chrome.storage.local.set({"encrypted_toggl_api_token": base64EncryptedToken});
                chrome.storage.local.set({'toggl_api_key': res['data']['api_token']});
                document.getElementById("login").style.display = 'none';
                document.getElementById("config").style.display = 'block';
                document.getElementById("buttons").style.display = 'block';
                getTogglWorkspaces(encodedCreds)
        },
        // If not, "Invalid credentials" is shown and user can´t start tracking time
        error: function(err) {
            loginButton.disabled = false;
            if (err.status === 403) {
                messageP.innerHTML = "Invalid credentials!";
            }
            else {
                messageP.innerHTML = "There was a problem with toggl. \n Try logging in again later."
            }
            messageP.style.display = 'block';
        }
    });
}

// Get your workspaces
function getTogglWorkspaces(encodedCreds) {
    $.ajax({
        type: "GET",
        // My own CORS proxy, because Chrome did not allow me to make a GET request
        url: "https://murmuring-basin-43724.herokuapp.com/https://www.toggl.com/api/v8/workspaces",
        beforeSend:
            function (xhr) {
                xhr.setRequestHeader('Authorization', "Basic "+encodedCreds);
        },
        // If successful, workspaces will be added to storage
        success:
            function(res) {
                let workspaces = {};
                for (let i=0 ; i<res.length ; i++) {
                    workspaces[res[i]['name']] = res[i]['id'];
                }
                chrome.storage.local.set({'workspaces': workspaces});
        },
        // If not, then something has gone wrong
        error: function(err) {
            console.log(err)
        }
    });
}