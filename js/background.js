chrome.runtime.onMessage.addListener(
	function(request, sender, sendResponse) {
		// Stop the timer and clear alarms, when break button is pressed
		if (request.msg === "stop_everything") {
			chrome.alarms.clearAll();
		}
	}
);

chrome.alarms.onAlarm.addListener(function(alarm) {
	// When timer starts running, an alarm with name of "break_alarm" gets created.
	// This means, that after x-amount (pomodoro length) of time, we need to rest
	if (alarm.name === 'break_alarm') {
		autoEndTogglTimer();
		chrome.storage.local.get(function(res) {
			let pomodoroCount = res["pomodoro_count"];
			// If it is every 4th pomodoro, we take a longer rest and create a "go_alarm"
			if ((pomodoroCount+1) % 4 === 0) {
				createAlarm("go_alarm", res["pomodoro_long_break"]);
			}
			// If it is not every 4th, we take shorter rest
			else {
				createAlarm("go_alarm", res["pomodoro_break"]);
			}
			let newPomodoroCount = pomodoroCount+1;
			chrome.storage.local.set({"pomodoro_count": newPomodoroCount});
		});
	}
	// When alarm "go_alarm" goes off, we start the timer again and create a "break_alarm"
	else if (alarm.name === 'go_alarm') {
		autoStartTogglTimer();
		chrome.storage.local.get(function(res) {
			createAlarm("break_alarm", res["pomodoro_length"]);
		});
	}
});

// A function to create an alarm with name and length
function createAlarm(alarmName, alarmLength) {
	chrome.alarms.create(alarmName, {when: Date.now() + alarmLength*60000})
}

function handleSuccessfulAutoStart(pomodoroLength, response) {
	// Set the time entry ID, for stopping timer later
	chrome.storage.local.set({"current_time_entry_id": parseInt(response["data"]["id"])});
	// Alerting and changing badge, to notify user, that timer has started
	chrome.browserAction.setBadgeText({text: 'ON'});
	let options = {
		body: "A pomodoro timer has been set for " + pomodoroLength + "minutes!"
	};
	new Notification("Timer has started", options);
}

// Starting toggl timer automatically
function autoStartTogglTimer() {
	chrome.storage.local.get(function (res) {
		let description = res['pomodoro_goal'];
		let workspaceId = res['selected_workspace'];
		let createdW = "Created with Chrome Extension";
		let encodedCreds = res['encrypted_toggl_api_token'];
		let pomodoroLength = res['pomodoro_length'];
		let jsonBody = JSON.stringify({"time_entry": {"description":description, "wid":workspaceId, "created_with":createdW}});

		$.ajax({
			url: "https://murmuring-basin-43724.herokuapp.com/https://www.toggl.com/api/v8/time_entries/start",
			method: "POST",
			contentType: "application/json",
			data: jsonBody,
			beforeSend:
				function (xhr) {
					xhr.setRequestHeader('Authorization', "Basic "+encodedCreds);
				},
			success: function (response) {
				handleSuccessfulAutoStart(pomodoroLength, response)
			},
			error: function (err) {
				timerPause("I could not get Toggl to respond to me. No timer for you!", "There has been an error with Toggl.");
				chrome.alarms.clearAll();
			}
		});
	});
}
// Ending toggl timer automatically
function autoEndTogglTimer() {
	chrome.storage.local.get(function(res) {
		let timerId = res["current_time_entry_id"];
		let stopUrl = "https://www.toggl.com/api/v8/time_entries/"+timerId+"/stop";
		let encodedCreds = res['encrypted_toggl_api_token'];

		$.ajax({
			url: "https://murmuring-basin-43724.herokuapp.com/"+stopUrl,
			method: "PUT",
			contentType: "application/json",
			beforeSend: function (xhr) {
				xhr.setRequestHeader('Authorization', "Basic "+encodedCreds)
			},
			success: function () {
				timerPause("Timer has been stopped.", "Time for a break!");
				// Time for kittens, when there is a break
				chrome.tabs.create({url:"html/kittens.html"});
			},
			error: function () {
				timerPause("Please go to https://toggl.com and stop your timer yourself!", "Time has run out and there has been an error. ");
				chrome.alarms.clearAll();
			}
		});
	});
}
// Alerting and setting badge to notify user, what has happened
function timerPause(text, title) {
	chrome.browserAction.setBadgeText({text: 'BREAK'});
	chrome.storage.local.set({"timer_running": false});
	let options = {
		body: text
	};
	new Notification(title, options);
}