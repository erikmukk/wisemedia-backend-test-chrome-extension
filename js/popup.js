let startButton;
document.addEventListener("DOMContentLoaded", function () {

	// Get buttons and elements
	let lengthSpan = document.getElementById("pomodoro_length");
	let breakSpan = document.getElementById("break_length");
	let longBreakSpan = document.getElementById("long_break_length");
	let higherLen = document.getElementById("higher_len");
	let lowerLen = document.getElementById("lower_len");
	let higherBreak = document.getElementById("higher_break");
	let lowerBreak = document.getElementById("lower_break");
	let higherLongBreak = document.getElementById("higher_long_break");
	let lowerLongBreak = document.getElementById("lower_long_break");
	startButton = document.getElementById("start_pomodoro_button");
	let stopButton = document.getElementById("stop_pomodoro_button");
	let configDiv = document.getElementById("config");
	let buttonsDiv = document.getElementById("buttons");
	let loginDiv = document.getElementById("login");
	let workspaces = document.getElementById("workspaces");
	let goal = document.getElementById("goal");
	let logoutButton = document.getElementById("logout");

	// Add eventListeners to buttons
	higherLen.addEventListener('click', function (e) {
		e.preventDefault();
		higher(lengthSpan);
	});
	lowerLen.addEventListener('click', function (e) {
		e.preventDefault();
		lower(lengthSpan);
	});
	higherBreak.addEventListener('click', function (e) {
		e.preventDefault();
		higher(breakSpan);
	});
	lowerBreak.addEventListener('click', function (e) {
		e.preventDefault();
		lower(breakSpan);
	});
	higherLongBreak.addEventListener('click', function (e) {
		e.preventDefault();
		higher(longBreakSpan);
	});
	lowerLongBreak.addEventListener('click', function (e) {
		e.preventDefault();
		lower(longBreakSpan);
	});
	startButton.addEventListener('click', function (e) {
		e.preventDefault();
		if (goal.value === '') {
			goal.style.backgroundColor = "#ff4242";
			goal.placeholder = "REQUIRED!";
		}
		else if (workspaces.options[workspaces.selectedIndex].value === "") {
			workspaces.style.backgroundColor = "#ff4242";
		}
		else {
			// Setting initial pomodoro count and configuration
			// Disabling start button after clicked once, so user couldn´t spam it and break it
			startButton.disabled = true;
			chrome.storage.local.set({
				"pomodoro_count": 0,
				"pomodoro_length": parseInt(lengthSpan.innerText),
				"pomodoro_break": parseInt(breakSpan.innerText),
				"pomodoro_long_break": parseInt(longBreakSpan.innerText),
				"selected_workspace": parseInt(workspaces.options[workspaces.selectedIndex].value),
				"pomodoro_goal": goal.value
			});
			startTogglTimer();
		}
	});
	stopButton.addEventListener('click', function (e) {
		e.preventDefault();
		// Disabling break button after clicked once, so user couldn´t spam it and break it
		stopButton.disabled = true;
		chrome.alarms.clearAll();
		endTogglTimer();
	});
	workspaces.addEventListener('click', function () {
		chrome.storage.local.get("workspaces", function (res) {
			if (workspaces.length === 1) {
				let temp = res['workspaces'];
				for (let key in temp) {
					let opt = document.createElement("option");
					opt.value = temp[key];
					opt.textContent = key;
					workspaces.appendChild(opt);
				}
			}
		})
	});
	logoutButton.addEventListener('click', function () {
		chrome.storage.local.clear();
		window.close();
	});

	// Setting correct badge, depending on the state of pomodoro
	chrome.browserAction.getBadgeText({}, function (res) {
		// If pomodoro is running or breaking, you can click to break it
		if (res === "ON" || res === "BREAK") {
			startButton.disabled = true;
			stopButton.disabled = false;
			configDiv.style.display = "none";
		}
		// If pomodoro is not running, you can start it
		else {
			startButton.disabled = false;
			stopButton.disabled = true;
		}
	});

	// Checking for toggl API token
	chrome.storage.local.get(function(res) {
		// If it is in storage, you don´t have to log in again
        if (res['toggl_api_key']) {
            loginDiv.style.display = 'none';
        }
        // Otherwise, you will be shown login form only
        if (!res['toggl_api_key']) {
        	configDiv.style.display = 'none';
        	buttonsDiv.style.display = 'none';
		}
    });
});

function higher(elem) {
	let len = parseInt(elem.innerHTML);
	len++;
	elem.textContent = len;
}
function lower(elem) {
	let len = parseInt(elem.innerHTML);
	if (len > 1) {
		len--;
		elem.textContent = len;
	}
}

// Function which ends the timer and notifies the user of that
function timerClosing(text, title) {
	// Clear pomodoro alarm, when manually broken
	chrome.alarms.clearAll();
	chrome.browserAction.setBadgeText({text: 'OFF'});
	chrome.runtime.sendMessage({
		msg: "stop_everything",
	});
	let options = {
		body: text
	};
	new Notification(title, options);
}

function handleSuccessfulStart(pomodoroLength, response) {
	// Set the time entry ID, for stopping timer later
	chrome.storage.local.set({"current_time_entry_id": parseInt(response["data"]["id"])});
	// Alerting and changing badge, to notify user, that timer has started
	chrome.browserAction.setBadgeText({text: 'ON'});
	// Make the first alarm to mark that pomodoro is running
	chrome.alarms.create("break_alarm", {when: Date.now()+pomodoroLength*60000});
	let options = {
		body: "A pomodoro timer has been set for " + pomodoroLength + "minutes!"
	};
	new Notification("Timer has started", options);
}

// Starting toggl timer manually
function startTogglTimer() {
	chrome.storage.local.get(function (res) {
		let description = res['pomodoro_goal'];
		let workspaceId = res['selected_workspace'];
		let createdW = "Created with Chrome Extension";
		let encodedCreds = res['encrypted_toggl_api_token'];
		let pomodoroLength = res['pomodoro_length'];
		let jsonBody = JSON.stringify({"time_entry": {"description":description, "wid":workspaceId, "created_with":createdW}});

		$.ajax({
			url: "https://murmuring-basin-43724.herokuapp.com/https://www.toggl.com/api/v8/time_entries/start",
			method: "POST",
			contentType: "application/json",
			data: jsonBody,
			beforeSend:
				function (xhr) {
					xhr.setRequestHeader('Authorization', "Basic "+encodedCreds);
				},
			success: function (response) {
				handleSuccessfulStart(pomodoroLength, response);
				window.close();
			},
			error: function (err) {
				timerClosing("You can not access that workspace! Try choosing another workspace.", "Timer was not started");
				window.close();
			}
		});
	});
}
// Ending toggl timer manually
function endTogglTimer() {
	chrome.storage.local.get(function(res) {
		let timerId = res["current_time_entry_id"];
		let stopUrl = "https://www.toggl.com/api/v8/time_entries/"+timerId+"/stop";
		let encodedCreds = res['encrypted_toggl_api_token'];

		$.ajax({
			url: "https://murmuring-basin-43724.herokuapp.com/"+stopUrl,
			method: "PUT",
			contentType: "application/json",
			beforeSend: function (xhr) {
				xhr.setRequestHeader('Authorization', "Basic "+encodedCreds)
			},
			success: function () {
				// Alerting and setting badge to notify user, that timer has stopped
				timerClosing("Timer has been stopped and reset", "A pomodoro has been broken!");
				window.close();
			},
			error: function (err) {
				// Alerting to notify user that timer has stopped
				if (err.status === 409 && err.responseJSON === "Time entry already stopped") {
					timerClosing("Timer has been stopped and reset", "A pomodoro has been broken")
				}
				else {
					timerClosing("Please go to 'https://toggl.com' and stop the timer yourself!",
						"There has been an error")
				}
				window.close();
			}
		});
	});
}