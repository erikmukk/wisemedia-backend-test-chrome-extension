let catButton = document.getElementById("catButton");

catButton.addEventListener('click', function (e) {
	e.preventDefault();
	getCats();
});

document.addEventListener("DOMContentLoaded", function () {
	let breakHeader = document.getElementById("break");
	chrome.storage.local.get(function(res) {
		let pomodoroCount = res["pomodoro_count"];
		let long_break = res["pomodoro_long_break"];
		let short_break = res["pomodoro_break"];
		// If it is every 4th pomodoro, we take a longer rest
		if (pomodoroCount % 4 === 0) {
			breakHeader.innerHTML = "Time for a " + long_break +"minute break!"
		}
		// If it is not every 4th, we take shorter rest
		else {
			breakHeader.innerHTML = "Time for a " + short_break +"minute break!"
		}
	});
});

function getCats() {
	$.ajax({
		url: 'https://api.thecatapi.com/v1/images/search?size=full',
		method: 'GET',
		success: function (response) {
			let img = '<img src="' + response[0]["url"] + '">';
			document.getElementById("image").innerHTML = img;
		},
		error: function (err) {
			let p = '<p>There has been a problem fetching cat pictures. Sorry for Your inconvenience.</p>';
			document.getElementById("image").innerHTML = p;
		}
	});
}